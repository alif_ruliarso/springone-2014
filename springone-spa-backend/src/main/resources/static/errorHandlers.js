$(function() {

    //some new browsers like to cache even api calls
    //this prevents such behaviour
    $.ajaxSetup(
        {
            cache:false
        }
    );

    $(document).ajaxStart(
        function ()
        {
            //do some stuff before every ajax request
        }
    );

    $(document).ajaxComplete(
        function ()
        {
        //do something after every ajax call
        }
    )

    $(document).ajaxSuccess(
        function (event, jqXHR, ajaxSettings, thrownError)
        {
            if (ajaxSettings.type == "POST")
            {
                //create success
            }
            if (ajaxSettings.type == "PUT")
            {
                 //edit success
            }
            if (ajaxSettings.type == "DELETE")
            {
                //delete success
            }
        }
    );

    $(document).ajaxError(
        function (event, jqXHR, ajaxSettings, thrownError)
        {
            var errMessage = "";
            if (_.isObject(jqXHR))
            {
                if (jqXHR.responseText) {
                    jqXHR = jqXHR.responseText;
                    var jqXHRResponse = '';
                    try
                    {
                        jqXHRResponse = $.parseJSON(jqXHR);
                    }
                    catch (e)
                    {
                        //not a json
                    }
                    if (_.isObject(jqXHRResponse)) {
                        jqXHR = jqXHRResponse;
                    }
                }
            }
            if (_.isObject(jqXHR) )
            {
                //
                // if session expired
                //
                if (jqXHR.apiCode == 'cdbfa4c6-9ae6-46da-aef7-fa167f689afe')
                {
                    $('#loginModal').modal('show');
                    return;
                }

                if (jqXHR.userMessage)
                {
                    //show message to user
                    errMessage ='<div class="alert alert-danger"><h4><i class="fa fa-times-circle"></i> Error</h4>'+ jqXHR.userMessage+'</div>';
                }
                if (jqXHR.devMessage)
                {
                   //notify admin
                }
            }
            else
            {
                //something went totally wrong
                errMessage = '<strong>We are sorry for not completing your request.<br/> There was an error with server communication. Please try again<br/><strong>';
            }
        }
    );

    //login modal handler
    $('#loginForm').submit(function () {

        $.ajax({
            type: 'POST',
            headers: {"X-spa-api-login": "true"},
            url: $('#loginForm').attr('action'),
            data: {username: 'user', password: 'password'},
            dataType: 'json'
        })
            .error(function (xhr) {
                alert('Wrong login credentials');
            })
            .success(function (obj) {
                $('#loginModal').modal('hide');
            })
            .complete(function () {

            });
        return false;
    });

});
