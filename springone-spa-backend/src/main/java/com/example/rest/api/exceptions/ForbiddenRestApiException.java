package com.example.rest.api.exceptions;

import com.example.rest.api.RestApiException;
import com.example.rest.api.RestApiHttpStatus;

public class ForbiddenRestApiException extends RestApiException
{
	private static final long serialVersionUID = 1L;

	public ForbiddenRestApiException()
	{
		super(RestApiHttpStatus.FORBIDDEN);
	}
}
