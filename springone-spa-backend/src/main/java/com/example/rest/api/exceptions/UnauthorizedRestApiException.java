package com.example.rest.api.exceptions;

import com.example.rest.api.RestApiException;
import com.example.rest.api.RestApiHttpStatus;

public class UnauthorizedRestApiException extends RestApiException
{
	private static final long serialVersionUID = 1L;

	public UnauthorizedRestApiException()
	{
		super(RestApiHttpStatus.UNAUTHORIZED);
	}
}
