package com.example.rest.client;

/**
 * This interface is used to determine if a rest login attempt was successful.
 * 
 * @author adib
 * 
 */
public interface LoginStatusChecker
{
	/**
	 * When performing a login it is expected that the login module will send an http redirect to the protected resource or stay on the
	 * login page if the login failed, while sending back a 200 OK.
	 * 
	 * By implementing this interface it is possible for client code to examine the redirect url to determine if the login was sucessfull or
	 * not.
	 * 
	 * @param redirectUrl
	 * @return
	 */
	public boolean isLoginSuccesfull(String redirectUrl);

}
